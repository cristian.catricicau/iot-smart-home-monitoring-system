
#include "ACS712.h"
#include <ZMPT101B.h>
#define SENSITIVITY 210.25f

ACS712 sensor_A(ACS712_30A, A1);

ZMPT101B voltageSensor(A0, 50.0);

void setup() 
{

  voltageSensor.setSensitivity(SENSITIVITY);
  delay(5000); 
}
 
void loop() {

  Serial.begin(9600);
  
  float I = sensor_A.getCurrentAC();
  
  float voltage = voltageSensor.getRmsVoltage();
   
   if (I < 0.10) 
    I = 0;
                     
      if (voltage< 10) 
    voltage = 0;
      
      float P = voltage * I ;

      Serial.println(String(" Voltage: ")+ voltage+(",") +String(" Current: ") + I +(",") + String(" Power: ") +P);
      
    
      delay(1000);
    }

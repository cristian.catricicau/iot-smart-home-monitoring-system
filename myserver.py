from flask import Flask, render_template
import time
from datetime import datetime
import Adafruit_DHT
import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BOARD)
GPIO.setup(37, GPIO.IN, pull_up_down = GPIO.PUD_DOWN)
import serial
import csv
import re
app = Flask(__name__)

DHT_SENSOR = Adafruit_DHT.DHT22
DHT_PIN = 4


@app.route("/")
def serve_homepage():
    
  
    input = GPIO.input(37)
    
    if (input == False):
        
        print("Clear")
        Ala = "Clear";
    elif (input == True):
        
        print("Alarm")
        Ala = "Alarm";
        
         
    with serial.Serial('/dev/ttyUSB0', 9600) as ser:
        
        line = ser.readline().decode('utf-8').rstrip()
        print(line)
        match = re.findall(r'\d+\.\d+',(line))
        if match:
         last_num=match[-1]
         print('last_nr:',last_num)
         file = open(r'temp', "r+")
         file.truncate(0)
         file.write(last_num)
         file.close()
         
         file = open(r't.txt', "a")
         file.write(last_num)
         file.write(datetime.today().strftime(' Time=%Y-%m-%d %H:%M:%S')+"\n")
         file.write("\n")
         file.close()
        
                    
        
    if line is not None:
        file = open("log.txt","a", encoding='utf-8')
        file.write(format(line))
        file.write(datetime.today().strftime(' Time=%Y-%m-%d %H:%M:%S')+"\n")
        file.close()
        
    humidity,temperature = Adafruit_DHT.read_retry(DHT_SENSOR, DHT_PIN)
    
    humidity = '{0:0.1f}'.format(humidity)
    temperature = '{0:0.1f}'.format(temperature)
    
    
    time = datetime.today().strftime('%Y-%m-%d %H:%M:%S')
    myData = {
      'Ala' : Ala,
      'tempVal' : temperature,
      'humidVal' : humidity,
      'line': line,
      'myTime' : time
    
     }
    return render_template('index_main.html', **myData)

if __name__ =="__main__":

 app.run(host='192.168.0.106', port=8000, debug=True)

